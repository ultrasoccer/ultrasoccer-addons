function convert2csv( delimiter=',' ){

  // init data array
  var B=[];

  // add csv headline
  B.push( [ 'Pl','Pl ex','TeamID','Team','S','U','N','Hist','T+','T-','Td','Pkt' ].join(delimiter) );

  // convert html table data to csv
  Object.values( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].rows ).forEach(
    function(v,i){
      var a=Object.values( v.children ).map( (w) =>  w );
      if( a.length == 1 ) return;
      var b=[];

      var aa=a[0].innerText.split(' ');
      if( aa.length != 2 ) return;

      b.push( aa[0].replace('.','')*1 );
      b.push( aa[1].replace('.','')*1 );

      if( a[1].getElementsByTagName('a').length == 1){
        b.push( a[1].getElementsByTagName('a')[0].href.split('=')[1] );
      } else {
        b.push('');
      }

      b.push( a[1].innerText.trim() );

      var aa=a[2].innerText.split('/');
      b.push( aa[0]*1 );
      b.push( aa[1]*1 );
      b.push( aa[2]*1 );

      if( a[3].children[0] != undefined ){
        var rgb=a[3].children[0].style.backgroundColor.replace('rgb(','').replace(')','').split(',');
        if( rgb[0]*1 > 0 && rgb[1]*1 > 0 ){
          b.push('U');
        } else if( rgb[0]*1 > 0 ){
          b.push('N');
        } else {
          b.push('S');
        }
      } else {
        b.push( '' );
      }

      var aa=a[4].innerText.split('/');
      b.push( aa[0]*1 );
      b.push( aa[1]*1 );

      b.push( a[5].innerText*1 );
      b.push( a[6].innerText*1 );

      B.push( b.join(delimiter) );
    }
  );

  return B.join('\n');

}

// append texarea
var newElement = document.createElement("textarea");
newElement.className='csv-export';
newElement.style.display='none';
newElement.style.backgroundColor='rgba(255,255,255,0.9)';
newElement.ondblclick=function(){
  this.style.display='none';
}
document.getElementById('gabBody').appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[,]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(',');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h1')[0].appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[;]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(';');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h1')[0].appendChild(newElement);
