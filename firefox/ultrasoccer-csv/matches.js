function convert2csv( delimiter=',' ){

  // init data array
  var B=[];

  // add csv headline
  B.push( [ 'Saison','Spieltag','HeimID','Heim','HeimPl','GastID','Gast','GastPl','H','A','matchID' ].join(delimiter) );

  // convert html table data to csv
  Object.values( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].rows ).slice(1).forEach(
    function(v,i){
      var a=Object.values( v.children ).map( (w) =>  w );
      if( a.length == 1 ) return;
      var b=[];

      b.push( document.getElementsByTagName('select')[2].value );

      b.push( document.getElementsByTagName('select')[3].value );

      if( a[0].getElementsByTagName('a').length == 1){
        b.push( a[0].getElementsByTagName('a')[0].href.split('=')[1] );
      } else {
        b.push('');
      }

      b.push( a[0].innerText.replace(/\([1-9]([1-9]|)\.\)/g,'').trim() );
      b.push( a[0].innerText.split('(')[1].split('.')[0] );


      if( a[1].getElementsByTagName('a').length == 1){
        b.push( a[1].getElementsByTagName('a')[0].href.split('=')[1] );
      } else {
        b.push('');
      }

      b.push( a[1].innerText.replace(/\([1-9]([1-9]|)\.\)/g,'').trim() );
      b.push( a[1].innerText.split('(')[1].split('.')[0] );

      var aa=a[2].innerText.split(':');
      b.push( aa[0]*1 );
      b.push( aa[1]*1 );

      if( a[2].getElementsByTagName('a').length == 1){
        b.push( a[2].getElementsByTagName('a')[0].href.split('=')[1] );
      } else {
        b.push('');
      }

      B.push( b.join(delimiter) );
    }
  );

  return B.join('\n');

}

// append texarea
var newElement = document.createElement("textarea");
newElement.className='csv-export';
newElement.style.display='none';
newElement.style['z-index']=100;
newElement.style.backgroundColor='rgba(255,255,255,0.9)';
newElement.ondblclick=function(){
  this.style.display='none';
}
document.getElementById('gabBody').appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[,]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(',');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h2')[0].appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[;]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(';');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h2')[0].appendChild(newElement);
