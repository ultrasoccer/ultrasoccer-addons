var historie_index = Object.entries( document.getElementsByClassName('gabBigTable') ).filter((a) => ( a[1].parentElement.innerText.match(/Karriere/g) != null )  )[0][0];
var karriereende = Object.entries( document.getElementsByClassName('gabHalfBox') ).filter((a) => ( a[1].parentElement.innerText.match(/Der Spieler hat seine Karriere beendet/g) != null )  ).length > 0;

function convert2csv(delimiter=','){

  // init data array
  var B=[];

  // add csv headline
  B.push([ 'SpielerID','Spieler','Alter','Nt','Saison','Liga','TeamID','Team','S','T','A','Z+','Z-','Zd','Bk','Bi+','Bi-', 'Attr', 'Face', 'Karriereende' ].join(delimiter) );

  // get age reference
  var tr=document.getElementsByClassName('gabBigTable')[ historie_index ].getElementsByTagName('tbody')[0].rows;
  var ref=tr[tr.length-1].cells[0].innerText*1;

  // convert html table data to csv
  var T=Object.values( document.getElementsByClassName('gabBigTable')[ historie_index ].getElementsByTagName('tbody')[0].rows ).slice(1);

  T.forEach(
    function(v,i){

      var a=Object.values( v.children ).map( (w) =>  w );
      if( a.length == 1 ) return;
      var b=[];

      b.push( document.URL.split('=')[1] ); // SpielerID
      b.push( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('b')[0].innerText ); // Spieler

      b.push( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].getElementsByTagName('tr')[1].getElementsByTagName('td')[1].innerText*1 + a[0].innerText*1 - ref ); // Alter

      b.push( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].rows[3].cells[1].getElementsByTagName('img')[0].src.split(/flag\-/g)[1].split(/\.png/g)[0] );

      b.push( a[0].innerText*1 ); // Saison
      b.push( a[1].innerText.trim() ); // Liga

      if( a[2].getElementsByTagName('a').length == 1){
        b.push( a[2].getElementsByTagName('a')[0].href.split('=')[1] ); // TeamID
      } else {
        b.push(''); // TeamID
      }
      b.push( a[2].innerText.trim() ); // Team

      var aa=a[3].innerText.split('/');
      b.push( aa[0]*1 ); // Spiele
      b.push( aa[1]*1 ); // Tore
      b.push( aa[2]*1 ); // Assists

      var aa=a[4].innerText.split('/');
      b.push( aa[0]*1 ); // Z+
      b.push( aa[1]*1 ); // Z-
      b.push( aa[0]*1 - aa[1]*1 ); // Zd

      if( delimiter == ',' ) {
        b.push( a[5].innerText.replace(',','.')*1 ); // Bk
      } else {
        b.push( a[5].innerText ); // Bk
      }

      var aa=a[6].innerText.split(':');
      b.push( aa[0]*1 ); // Bi+
      b.push( aa[1]*1 ); // Bi-

      //b.push( a[7].innerText.replace(',','').replace(/.k/g,'000').replace(/.M/g,'0000') ); // Mw

      b.push( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].getElementsByTagName('tr')[0].getElementsByTagName('i')[0]?.className ); // Spielerattribute

      var face=document.getElementById('firstInfoTable').getElementsByTagName('tbody')[0].getElementsByTagName('tr')[0].getElementsByTagName('td')[0].getElementsByTagName('div')[0].children[0].children;

      var fg_head =face[0].src.split(/fg_head_/g)[1].split(/\./g)[0];
      var fg_hair =face[1].src.split(/fg_hair_/g)[1].split(/\./g)[0];
      var fg_beard=face[2].src.split(/fg_beard_/g)[1].split(/\./g)[0];
      var fg_mouth=face[3].src.split(/fg_mouth_/g)[1].split(/\./g)[0];
      var fg_eyes =face[4].src.split(/fg_eyes_/g)[1].split(/\./g)[0];

      b.push( fg_head + '_' + fg_hair + '_' + fg_beard + '_' + fg_mouth + '_' + fg_eyes );

      i == T.length - 1 && karriereende ? b.push(1) : b.push(0);
      B.push(b.join(delimiter));
    }
  );

  return B.join('\n');

}

// append texarea
var newElement = document.createElement("textarea");
newElement.className='csv-export';
newElement.style.display='none';
newElement.style['z-index']=100;
newElement.style.backgroundColor='rgba(255,255,255,0.9)';
newElement.ondblclick=function(){
  this.style.display='none';
}
document.getElementById('gabBody').appendChild(newElement);


// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[,]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(',');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabBigTable')[ historie_index ].parentElement.getElementsByTagName('p')[0].appendChild(newElement);


// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[;]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(';');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabBigTable')[ historie_index ].parentElement.getElementsByTagName('p')[0].appendChild(newElement);
