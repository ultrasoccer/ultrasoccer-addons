function convert2csv( delimiter=',' ){

  // init data array
  var B=[];

  // add csv headline
  B.push([ 'TeamID','N','SpielerID','Attr','Spieler','Alter','Pos','S','T','A','Z+','Z-','Zd' ].join(delimiter));

  // convert html table data to csv
  Object.values( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].rows ).slice(1).forEach(
    function(v,i){
      var a=Object.values( v.children ).map( (w) =>  w );
      if( a.length == 1 ) return;
      var b=[];

      b.push( document.URL.split('=')[1] );

      if( a[0].getElementsByTagName('img').length == 1){
        b.push( a[0].getElementsByTagName('img')[0].src.split('flag-')[1].split('.png')[0] );
      } else {
        b.push('');
      }

      if( a[1].getElementsByTagName('a').length == 1){
        b.push( a[1].getElementsByTagName('a')[0].href.split('=')[1] );
      } else {
        b.push('');
      }

      if( a[1].getElementsByTagName('i').length > 0){
        var T=[]
        for( i=0; i<a[1].getElementsByTagName('i').length; i++ ){
          T.push(  a[1].getElementsByTagName('i')[i].classList[1] );
        }
        b.push( T.join(' ') );
      } else {
        b.push('');
      }

      b.push( a[1].innerText.trim() );
      b.push( a[2].innerText*1 );
      b.push( a[3].innerText.trim() );

      var aa=a[4].innerText.split('/');
      b.push( aa[0]*1 );
      b.push( aa[1]*1 );
      b.push( aa[2]*1 );

      var aa=a[5].innerText.split('/');
      b.push( aa[0]*1 );
      b.push( aa[1]*1 );
      b.push( aa[0]*1 - aa[1]*1 );

      //b.push( a[6].attributes.sortkey.value );

      B.push( b.join(delimiter) );
    }
  );

  return B.join('\n');

}

// append texarea
var newElement = document.createElement("textarea");
newElement.className='csv-export';
newElement.style.display='none';
newElement.style['z-index']=100;
newElement.style.backgroundColor='rgba(255,255,255,0.9)';
newElement.ondblclick=function(){
  this.style.display='none';
}
document.getElementById('gabBody').appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[,]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(',');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h1')[0].appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[;]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(';');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h1')[0].appendChild(newElement);
