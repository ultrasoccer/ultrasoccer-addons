function convert2csv( delimiter=',' ){

  // init data array
  var B=[];

  // add csv headline
  B.push( [ 'Saison','Spieltag','MatchID','teamID','L','Pos','SpielerID','Spieler','Z+','Z-','T','A','Bk' ].join(delimiter) );

  // convert html table data to csv
  var index=0;
  do {
    Object.values( document.getElementsByClassName('gabBigTable')[index].getElementsByTagName('tbody')[0].rows ).slice(1).forEach(
      function(v,i){
        var a=Object.values( v.children ).map( (w) =>  w );
        if( a.length == 1 ) return;
        var b=[];

        // Saison
        try { b.push( document.getElementsByClassName('gabContent')[0].innerText.split('Saison')[1].split(',')[0].trim() ); } catch (e) { b.push( 0 ); }

        // Spieltag
        try { b.push( document.getElementsByClassName('gabContent')[0].innerText.split('Spieltag')[1].split('\n')[0].trim() ); } catch (e) { b.push( 0 ); }

        // matchID
        b.push( document.URL.split('=')[1].split('&')[0] );

        // teamID
        b.push(  ( index == 0 ) ? document.getElementsByTagName('h2')[0].getElementsByTagName('a')[0].href.split('=')[1] : document.getElementsByTagName('h2')[0].getElementsByTagName('a')[1].href.split('=')[1] );

        // Heim - Gast
        b.push( ( index == 0 ) ? 'H' : 'G' );

        // Position
        b.push( a[0].innerText*1 );

        // spielerID
        if( a[1].getElementsByTagName('a').length == 1){
          b.push( a[1].getElementsByTagName('a')[0].href.split('=')[1] );
        } else {
          b.push('');
        }

        // Spielername
        b.push( a[1].innerText.trim() );

        // Zweikampfbilanz
        var aa=a[2].innerText.split('/');
        b.push( aa[0]*1 );
        b.push( aa[1]*1 );

        // Tore
        b.push( a[3].innerText*1 );

        // Assists
        b.push( a[4].innerText*1 );

        // Ballkontakte
        b.push( a[5].innerText*1 );

        // Zeile zum CSV Array hinzufügen
        B.push( b.join(delimiter) );
      }
    );
    index++;
  } while( index < 2 );

  return B.join('\n');

}

// append texarea
var newElement = document.createElement("textarea");
newElement.className='csv-export';
newElement.style.display='none';
newElement.style['z-index']=100;
newElement.style.backgroundColor='rgba(255,255,255,0.9)';
newElement.ondblclick=function(){
  this.style.display='none';
}
document.getElementById('gabBody').appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[,]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(',');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h2')[0].appendChild(newElement);

// append button
var newElement = document.createElement("button");
newElement.className='csv-export-btn';
newElement.innerText='csv[;]';
newElement.onclick=
  function(){
    document.getElementsByClassName('csv-export')[0].value = convert2csv(';');
    document.getElementsByClassName('csv-export')[0].style.display='';
  };
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h2')[0].appendChild(newElement);
