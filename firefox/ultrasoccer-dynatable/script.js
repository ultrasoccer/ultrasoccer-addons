function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function dynaTable(inaktiv=false){

  var p=-1;

  // iterate game table elements
  Object.values( document.getElementsByClassName('gabBigTable')[0].getElementsByTagName('tbody')[0].rows ).forEach(
    function(v,i){
      var tr=v.children;
      if( tr[0].localName != 'td' ) return;
      if( tr.length == 6 || tr.length == 7 ){
        var pi=tr.length - 1;
        if( p == -1 ) p=tr[pi].innerText;
        if( inaktiv ) {
          // reset row height to origin height
          v.style.height='26px';
        } else {
          // recalculate new row height by game score difference
          v.style.height=((p-tr[pi].innerText+1)*8)+'px';
        }
        v.style['vertical-align']='bottom!important';

        // get game score for next row height recalculation
        p=tr[pi].innerText;
      }
    }
  );

  // add a button to switch dynatable extension on/off
  var ds=document.getElementsByClassName('dynatable-switch')[0];
  ds.innerText='Erweiterung '+(( inaktiv ) ? 'inaktiv' : 'aktiv' );
  ds.className='dynatable-switch '+(( inaktiv ) ? 'inaktiv' : 'aktiv' );

}


// use cookie to get/save if dynatable extension is on/off
var dynaTableMode=( getCookie('dynaTableMode') === 'true' );
var newElement = document.createElement("button");
newElement.className='dynatable-switch';
newElement.onclick=
  function(){
    // switch dynatable extension on/off
    dynaTableMode=!dynaTableMode;
    // store the last on/off setting to cookie
    setCookie('dynaTableMode',dynaTableMode,30);
    // recalculate row height
    dynaTable(dynaTableMode);
  };

// append button
document.getElementsByClassName('gabContent')[0].getElementsByTagName('h1')[0].appendChild(newElement);

// run script initially
dynaTable(dynaTableMode);
