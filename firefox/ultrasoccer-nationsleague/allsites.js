// add icon and link to navbar
var newElement = document.createElement("hr");
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[1].before(newElement);

var newElement = document.createElement("h5");
newElement.innerText='Erweiterung';
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[2].before(newElement);

var newElement = document.createElement("a");
newElement.innerText='[?]';
newElement.href=''

var newElement = document.createElement("i");
newElement.className='fas fa-globe';
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[2].before(newElement);

var newElement = document.createElement("a");
newElement.innerText='Nations-League';
newElement.className='gabNavLink';
newElement.target='fairplayground';
newElement.title='Verlinkt zur externen Webseite usnl.fairplayground.info, wird verwaltet vom Nutzer Tony Ford';
newElement.href='https://usnl.fairplayground.info/';
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[2].before(newElement);

var newElement = document.createElement("br");
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[2].before(newElement);

var newElement = document.createElement("p");
newElement.className='usnl-link';
newElement.innerText='[externe Seite]';
newElement.title='Verlinkt zur externen Webseite usnl.fairplayground.info, wird verwaltet vom Nutzer Tony Ford';
document.getElementsByClassName('gabNav')[0].getElementsByTagName('hr')[2].before(newElement);
