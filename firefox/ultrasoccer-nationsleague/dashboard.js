// search (NL) and replace team flag
for( var tbl=0; tbl<3; tbl++ ){
  Object.values( document.getElementsByClassName('gabBigTable')[tbl].rows ).slice(0).forEach(
    function(tr,i){
      var l = tr.cells[2].innerText.trim().split('(NL)');
      if( l.length == 2 ){
        var lch = NL_TEAMS[ l[0] + '(NL)' ];
        tr.cells[1].children[0].style.backgroundImage='url("../gfx/flags/flag-'+lch+'.png")';
      }
      var l = tr.cells[4].innerText.trim().split('(NL)');
      if( l.length == 2 ){
        var lch = NL_TEAMS[ l[0] + '(NL)' ];
        tr.cells[3].children[0].style.backgroundImage='url("../gfx/flags/flag-'+lch+'.png")';
      }
    }
  );
}
