// search (NL) and replace team flag
Object.values( document.getElementsByClassName('gabBigTable')[0].rows ).slice(1).forEach(
  function(tr,i){

    var lch = NL_TEAMS[ tr.cells[0].innerText.replace(/\([1-9]([0-9]|)\.\)/g,'').trim() ];
    if( lch != undefined ){
      tr.cells[0].getElementsByTagName('div')[0].style.backgroundImage='url("../gfx/flags/flag-'+lch+'.png")';
    }

    var lcg = NL_TEAMS[ tr.cells[1].innerText.replace(/\([1-9]([0-9]|)\.\)/g,'').trim() ];
    if( lcg != undefined ){
      tr.cells[1].getElementsByTagName('div')[0].style.backgroundImage='url("../gfx/flags/flag-'+lcg+'.png")';
    }

  }
);
