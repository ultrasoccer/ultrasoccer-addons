// search (NL) and replace team flag
Object.values( document.getElementsByClassName('gabBigTable')[0].rows ).slice(1).forEach(
  function(tr,i){
    if( tr.cells.length < 2 ) return;
    var lch = NL_TEAMS[ tr.cells[1].innerText.trim() ];
    if( lch != undefined ){
      tr.cells[1].children[0].style.backgroundImage='url("../gfx/flags/flag-'+lch+'.png")';
    }

  }
);
