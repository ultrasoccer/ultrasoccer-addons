// search (NL) and replace team flag
Object.values( document.getElementsByClassName('gabFlagLeft') ).forEach(
  function(v,i){
    var l=v.nextElementSibling.innerText.trim().split('(NL)');
    if( l.length == 2 ){
      var lc=NL_TEAMS[ l[0]+'(NL)' ];
      v.src='../gfx/flags/flag-'+lc+'.png';
    }
  }
);
