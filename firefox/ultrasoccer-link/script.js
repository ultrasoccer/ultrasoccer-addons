var P = document.getElementsByClassName('gabTextBoxContent');

Object.values(P).forEach(
  function(v){
    Object.values( v.getElementsByTagName('i') ).forEach(
        function(w){
            var t=w.innerText.trim();
            if( Number.isInteger(t*1) ){
                if( t > 0 && t < 2792 ){
                    var a=document.createElement('a');
                    a.href='https://ultrasoccer.de/players/?id=' + t
                    a.innerText = t;
                    w.innerText = '';
                    w.appendChild(a);
                } 
            } else if( /[0-9A-Fa-f]{32}/g.test(t) == true ){
                var a=document.createElement('a');
                a.href='https://ultrasoccer.de/playerprofile/?id=' + t
                a.innerText = t;
                w.innerText = '';
                w.appendChild(a);
            }
        }
    );
    Object.values( v.getElementsByTagName('span') ).forEach(
        function(w){
            var t = w.innerText.trim();
            var re=/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;
            if( re.test(t) ){
                var a=document.createElement('a');
                a.href = t
                a.innerText = t;
                a.target = 'extern';
                w.innerText = '';
                w.appendChild(a);
            }
        }
    );
  }  
);