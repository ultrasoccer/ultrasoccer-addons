# ultrasoccer-addons


## Firefox / ultrasoccer-dynatable


#### Beschreibung

Diese Firefox Erweiterung erlaubt es dem Manager die Punkteabstände über verschiedene Zeilenhöhen betrachten zu können.

#### Aktivierung / Deaktivierung

Die Erweiterung kann innerhalb der Tabellenseite über einen Button aktiviert und deaktiviert werden.

#### Cookies

Es wird das Cookie namens **dynaTableMode** verwendet, welches den Zustand der Erweiterung ( true/false ) speichert. Dadurch kann der letzte Schaltzustand, beim erneuten Laden der Seite oder Auswahl einer anderen Tabelle, erhalten bleiben.

Die Speicherdauer des Cookies beträgt 30 Tage.


## Firefox / ultrasoccer-csv

#### Beschreibung

Diese Firefox Erweiterung erlaubt es dem Nutzer die Tabelle, die Spielbegegnungen, die einzelnen Spiele und die Teams im csv-Format exportieren zu können, z.B. um Statistiken erstellen / erzeugen zu können.

#### Anwendung

Auf jeder der genannten Seiten wird mit Installation dieser Erweiterung zwei Buttons integriert.
+ `csv[,]` - csv mit Komma als Trennzeichen und Punkt als Dezimalzeichen.
+ `csv[;]` - csv mit Semikolon als Trennzeichen und Komma als Dezimalzeichen.


## Firefox / ultrasoccer-nationsleague

#### Beschreibung

Diese Firefox Erweiterung fügt einen externen Link zum Menü hinzu, welches bei Klick ein weiteres Browserfenster öffnet und die Seite https://usnl.fairplayground.info lädt, auf der die Spielbegegnungen, Ergebnisse und Tabellen der community-basierenden Ultrasoccer-NationsLeague zu finden sind.

Darüber hinaus werden die Teambanner durch die Länderflaggen ersetzt, so dass die NL Teams leichter wahrgenommen werden können. 
